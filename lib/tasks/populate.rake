#encoding: utf-8
namespace :db do
	desc "Fill database with sample data"
	task :populate => :environment do
		#Rake::Task['db:reset'].invoke

		require 'populator'
		require 'faker'
		require 'ryba'

		# Типы композиций:
		# 1 - Музыка без слов 
		# 2 - Музыка со словами
		# 3 - Песня

		genres = ['Рок', 'Классика', 'Хип-хоп', 'Инди', 'Национальная', 'Детская', 'R\'n\'B', 'Популярная']
		#genres.length.times do |n|
		#	Genre.create!(:g_name => genres[n])
		#end

		instruments = ['Гитара', 'Ударные', 'Пианино', 'Скрипка', 'Аккордион', 'Вокал', 'Виолончель', 'Балалайка']
		#instruments.length.times do |n|
		#	Instrument.create!(:i_name => instruments[n])
		#end

		# Создание юзеров
			#iam = User.create!(:email => "winogradovaa@gmail.com",
			#				  :password => "0a0a0a",
			#				  :password_confirmation => "0a0a0a")

			#my_account = iam.create_account(:f_name => "Антон",
			#							    :l_name => "Виноградов",
			#							    :birthday => Date.new(1990, 11, 2),
			#							    :account_type => 1,
			#							    :about => "А это я.")
			#5.times do |n|
			#	cover = [*1..50].sample
			#	genre = [*1..8].sample
			#	free_album = Album.create!(:a_name => Populator.words(1..3),
			#							   :author => Ryba::Name.first_name(male = true) + " " + Ryba::Name.family_name(male = true),
			#							   :genre => genres[genre],
			#							   :description => Populator.words(10..20),
			#							   :library_id => iam.library.id,
			#							   :cover => "images/covers/" + cover.to_s + ".jpg",
			#							   :upload_id => iam.upload.id)
			#	12.times do |m|
			#		instrument = [*1..8].sample
			#		free_album.compositions << Composition.create!(:c_name => Populator.words(1..3),
			#													   :genre => genres[genre],
			#													   :instrument => instruments[instrument],
			#													   :author => Ryba::Name.first_name(male = true) + " " + Ryba::Name.family_name(male = true),
			#													   :library_id => iam.library.id,
			#													   :upload_id => iam.upload.id)
			#	end
			#	instrument = [*1..8].sample
			#	iam.library.compositions << Composition.create!(:c_name => Populator.words(1..3),
			#													:genre => genres[genre],
			#													:instrument => instruments[instrument],
			#													:author => Ryba::Name.first_name(male = true) + " " + Ryba::Name.family_name(male = true),
			#													:library_id => iam.library.id,
			#													:upload_id => iam.upload.id)
			#end
			# Левые юзеры
			50.times do |n|
				user = User.create!(:email => Faker::Internet.email,
								   :password => "thisisfake",
								   :password_confirmation => "thisisfake")
				account = user.create_account(:f_name => Ryba::Name.first_name(male = true),
										      :l_name => Ryba::Name.family_name(male = true),
										      :birthday => Date.new(1990, 11, 2),
										      :account_type => 1,
										      :about => Populator.words(10..20))
				# Бесплатные альбомы
				8.times do |n|
					cover = [*1..50].sample
					genre = [*1..8].sample
					free_album = Album.create!(:a_name => Populator.words(1..3),
											   :author => Ryba::Name.first_name(male = true) + " " + Ryba::Name.family_name(male = true),
											   :genre => genres[genre],
											   :shop_id => user.shop.id,
											   :active => true,
											   :description => Populator.words(10..20),
											   :library_id => user.library.id,
											   :cover => "images/covers/" + cover.to_s + ".jpg",
											   :upload_id => user.upload.id)
					12.times do |m|
						instrument = [*1..8].sample
						free_album.compositions << Composition.create!(:c_name => Populator.words(1..3),
																	   :genre => genres[genre],
																	   :instrument => instruments[instrument],
																	   :author => Ryba::Name.first_name(male = true) + " " + Ryba::Name.family_name(male = true),
																	   :shop_id => user.shop.id,
																	   :library_id => user.library.id,
																	   :active => true,
																	   :upload_id => user.upload.id)
					end
				end
			end
		# end
	end
end