module Models
	module ItemsMethods
		def self.included(base)
			base.instance_eval do
				#before_create :init

				serialize :preferences

				belongs_to :purchase
	  			belongs_to :cart
	  			belongs_to :upload
	  			belongs_to :shop
	  			belongs_to :library

	  			scope :free, where("cost = 0")
	  			scope :paid, where("cost != 0")
	  			scope :active, where(deleted: false)
	  			scope :deleted, where(deleted: true)

	  			scope :sale, where(active: true, paused: false)

			end
		end

		module InstanceMethods
			def short_composer
				if self.author[0].chr != " "
	    			return self.author[0].chr+"."+" "+self.author.split(' ')[1] if self.author.split(' ').count > 1
	    			return self.author.split(' ')[0] if self.author.split(' ').count <= 1
	    		else
	    			return self.author[1].chr+"."+" "+self.author.split(' ')[1] if self.author.split(' ').count > 1
	    			return self.author.split(' ')[0] if self.author.split(' ').count <= 1
	    		end
  			end

			def free?
			   return true if self.cost == 0
		  	end

		  	def sale?
		  	   return true if self.active?
		  	end

			def increate_view
			   self.v_count += 1
			   self.save
			end

			def increate_buy
			   self.b_count += 1
			   self.save
			end

			private
				def init
					self.preferences ||= Hash.new
				end
		end

		module HistoryMethods

			def new_activity(who, character, message)
				Activity.create(from: who, character: character, message: message)
			end
		
		end
	end
end