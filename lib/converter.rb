    ##########################################################################
	# КОНВЕРТЕР
	##########################################################################
	LOGO = Rails.root.join('public', 'assets', 'logo_mini.png').to_s
    FOLDER = Rails.root.join('public','assets', 'users').to_s
    COMPOSITIONS = "/compositions/"

    XML_EXT = ".xml"
    LY_EXT = ".ly"
    PDF_EXT = ".pdf"
    JPG_EXT = ".jpg"
    JPEG_EXT = ".jpeg"
    PNG_EXT = ".png"

    DASH = " "
    SLASH = "/" 

    LILYPOND_FORMAT = "~/Downloads/LilyPond.app/Contents/Resources/bin/musicxml2ly"
    PDF_FORMAT = "~/Downloads/LilyPond.app/Contents/Resources/bin/lilypond"
    OUTPUT_COMMAND = "-o"

    # КОНВЕРТАЦИЯ XML в PDF
    # Сначала запускаем прогу для конвертации xml в ly формат. После чего конвертируем ly в pdf программой
    # lilypond. Создаем файлы предпросмотра и печатную версию. Удаляем сконвертированный pdf.
    #
    # Параметры:
    #   composition - композиция, чей xml конвертируем
    #   user - id юзера, того кому композиция принадлежит
    #
    def convert_to_pdf(composition, user)
        path = FOLDER + SLASH + user + COMPOSITIONS + composition + SLASH
        @composition.preferences[:folder] = path
        # convert with lilipond app
        Kernel.system LILYPOND_FORMAT + DASH + path + composition + XML_EXT + DASH + OUTPUT_COMMAND + DASH + path + composition + LY_EXT
        # convert with lilipond app
        Kernel.system PDF_FORMAT + DASH + OUTPUT_COMMAND + DASH + path + composition + DASH + path + composition + LY_EXT
        filename = path + composition + PDF_EXT # полученный pdf
        # создание превью pdf со штампами
        make_preview(path, filename, @composition)
        make_print(path, filename, @composition)
        # Удаление исходного файла pdf
        File.delete(path + filename) # рабочий pdf
    end


    # СОЗДАНИЕ ФАЙЛОВ ИЗОБРАЖЕНИЙ ДЛЯ ПРЕДПРОСМОТРА
    # Создаем файл pdf с наложенными штампами и копирайтами, после конвертируем его в изображения при помощи
    # ImageMagick. Обновляем параметры композиции. Удаляем pdf со штампами.
    #
    # Параметры
    #   path - расположение композиции
    #   filename - pdf файл для обработки
    #   composition - ссылка на композицию, чей файл мы обрабатываем
    #
    def make_preview(path ,filename, cid, user)
        original_pdf_file_name = path + filename        
        preview_pdf_file_name = path + "preview.pdf"

        pdf_processing(original_pdf_file_name, 0, preview_pdf_file_name)

        # Конвертация pdf превью в JPG для превью
        count = %x[identify -format %n #{preview_pdf_file_name.to_s}]
        FileUtils.makedirs(path + "preview")

        if count.to_i < 5 
            preview_count = 1
        else 
            preview_count = 3
        end

        preview_count.times do |n|
            Kernel.system "convert -density 400 #{preview_pdf_file_name.to_s}[#{n}] #{path + 'preview' + (n+1).to_s}.jpg"
        end

        c = Composition.find(cid)
        c.preferences[:preview_path] = "/assets/users/#{user}" + COMPOSITIONS + cid + SLASH + "preview" + SLASH
        c.preferences[:preview_count] = preview_count.to_s
        c.save
        # Удаление файла pdf превью, из которого мы получили изображения
        File.delete(preview_pdf_file_name)
    end


    # СОЗДАНИЕ ПЕЧАТНОЙ ВЕРСИИ
    # Создаем печатную версию файла pdf и обновляем параметры композиции
    #
    # Параметры
    #   path - расположение композиции
    #   filename - pdf файл для обработки
    #   composition - ссылка на композицию, чей файл мы обрабатываем
    #
    def make_print(path, filename, cid)
        original_pdf_file_name = path + filename  
        print_pdf_file_name = path + "print.pdf"

        pdf_processing(original_pdf_file_name, 1, print_pdf_file_name)

        c = Composition.find(cid)
        c.preferences[:print_file] = print_pdf_file_name.to_s
        c.save
    end


    # ОБРАБОТКА PDF
    # Если флаг равен 0 - создаем штамп и расставлем его на всех страницах, 8 раз по 5 штук в строке.
    # Далее ставим копирайты на всех старницах внизу страницы по центру. Создаем белый блок на всю ширину
    # страницы 550px и высотой 30px, ставим его с отступом от нижнего края в 20px и от левого 0, 
    # а сверху него пишем текст.
    #
    # Парметры:
    #   input - файл шаблона, исходный загруженный pdf
    #   flag - тип обработки
    #   output - обработанный файл
    #
    def pdf_processing(input, flag, output)
        Prawn::Document.new(:template => input) do 
          	if flag == 0
            	create_stamp("itnotes") do
              		image LOGO, :scale => 0.5
            	end
	            page_count.times do |n|
	              	go_to_page(n + 1)
	              	# Штамп
	              	8.times do |l|
	                	y = l*100
	                	5.times do |m|
	                  		x = (m*100)+40
	                  		transparent(0.3) { stamp_at "itnotes", [x, -y] }
	                	end
	              	end
	            end
          	end 

          	page_count.times do |n|
                go_to_page(n + 1)
            	# Ставим свои копирайты
            	fill_color "FFFFFF"
            	fill_rectangle [0,20], 550, 30
            	fill_color "000000"
            	text "© ITNotes 2012. All Rights reserved." , :align => :center, :valign => :bottom
          	end
          	render_file output
        end
    end