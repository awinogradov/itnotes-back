//= require jquery
//= require bootstrap
//= require chosen-jquery
//= require jquery.idealforms.min
//= require jquery_ujs
//= require_self

$(document).ready(function() {	

	$.rails.allowAction = function(link) {
		  if (!link.attr('data-confirm')) {
		  	return true;
		  }
		  $.rails.showConfirmDialog(link);
		  return false;
	}

	$.rails.confirmed = function(link) {
	  link.removeAttr('data-confirm');
	  return link.trigger('click.rails');
	}

   	$.rails.showConfirmDialog = function(link) {
	  var html, message;
	  message = link.attr('data-confirm');
	  html = "	    	<div class=\"modal confirmDialog\" id=\"confirmationDialog\">\n	    		<div class=\"modal-header\">\n		<a class=\"close\" data-dismiss=\"modal\">×</a>\n		<h4> ITNotes. Подтверждение. </h4>\n	</div>\n	<div class=\"modal-body\">\n		<p>"+ message +"</p>\n	</div>\n	<div class=\"modal-footer\">\n		<a data-dismiss=\"modal\" class=\"btn\">Отмена</a>\n		<a data-dismiss=\"modal\" class=\"btn btn-danger confirm\">Да</a>\n	</div>\n</div>";
	  $(html).modal();
	  return $('#confirmationDialog .confirm').on('click', function() {
	    return $.rails.confirmed(link);
	  });
	}

	function cancelEvent(e) {
  		if(e.preventDefault)e.preventDefault();
  		else e.returnValue=false;
	}

	$(".icon-bullhorn").click(
		function (e) {
			cancelEvent(e);
		}
	);




	var menu = getCookie("menu-open");
	if (menu === "yes") {
		$("li.included").show();
		$("#library-link").css("margin-bottom", "0");
	}

	$("#library-link").toggle(
		function () {
			if (menu === "no") {
				$("li.included").fadeIn(320);
				$(this).css("margin-bottom", "0");
				setCookie("menu-open", "yes", "", "/");
			}
			else {
				$("li.included").fadeOut(320);
				$(this).css("margin-bottom", "3px");
				setCookie("menu-open", "no", "", "/");
			}
		},
		function () {
			$("li.included").fadeOut(320);
			$(this).css("margin-bottom", "3px");
			setCookie("menu-open", "no", "", "/");
		}
	);
	
	$(".chzn-select").chosen();
 });

function setCookie (name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

function getCookie(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}

// ВАЛИДАЦИИ
$(document).ready(function() {

	// ВАЛИДАЦИИ СОЗДАНИЯ КОМПОЗИЦИИ
	var composition_options = {
		inputs: {
			'composition[cover]': {
				filters: 'extension',
				data: {extension: ['jpg','png'] },
				errors: { extension: "Поддерживается только форматы JPG, PNG"} 
			},
			'composition[audio]': {
				filters: 'extension',
				data: {extension: ['mp3'] },
				errors: { extension: "Поддерживается только формат MP3"} 
			},
			'composition[file]': {
				filters: 'extension',
				data: { extension: [ 'pdf' ]}
			},
			'composition[c_name]': {
				filters: 'required',
				errors: { required: "Укажите название композиции"}
			},
			'composition[author]': {
				filters: 'required',
				errors: { required: "Укажите автора композиции"}
			}
		}
	}

	var $composition = $('#composition').idealforms( composition_options ).data('idealforms');

	$('#instrument').parent().children('.ideal-icon-invalid').addClass('custom-error').show();
	$('#instrument').parent().parent().children('.ideal-error').html("Укажите инструмент");
	$('#genre').parent().children('.ideal-icon-invalid').addClass('custom-error').show();
	$('#genre').parent().parent().children('.ideal-error').html("Укажите жанр");

	$('.custom-error').click(function (){
		$(this).parent().parent().children('.ideal-error').toggle();
		$(this).parent().addClass('invalid');
	});

	$('#composition_author').parent().addClass('valid');
	$('#cost').parent().addClass('valid');
	$('#cost').parent().children('.ideal-icon-valid').show();


	$('#fake-btn').click(function() {
		yey_composition(); 
		return false;
	});

	function yey_composition() {
		if ($composition.isValid()) {
			if ($('#composition_file').val() == "") {
				$('#composition_file').parent().addClass('invalid');
				alert("Вы забыли добавить файл с композицией!");
			}
			else if ($('#instruments-group .instrument .ideal-select-title').html() == "") {
				$('#instrument').parent().addClass('invalid');
				$('#instrument').parent().children('.ideal-icon-invalid').click();
				$('#instrument').parent().parent().children('.ideal-error').show();
			}
			else if ($('#genres-group .genre .ideal-select-title').html() == "") {
				$('#genre').parent().addClass('invalid');
				$('#genre').parent().children('.ideal-icon-invalid').click();
				$('#genre').parent().parent().children('.ideal-error').show();
			}
			else {
				window.alert = function() { 
					$('#create-btn').addClass('btn-success').html("Зугрузка...").click(function () {
						$('.msg').html('Дождитесь загрузки композиции');
					});
				};
				$('#composition').submit();
			}
		}
		else {
			$composition.focusFirstInvalid();
		}
	}


	// ВАЛИДАЦИИ СОЗДАНИЯ СБОРНИКА
	var album_options = {
		inputs: {
			'album[file]': {
				filters: 'extension',
				data: {extension: ['jpg','png'] },
				errors: { extension: "Поддерживается только форматы JPG, PNG"} 
			},
			'album[a_name]': {
				filters: 'required',
				errors: { required: "Укажите название сборника"}
			},
			'album[author]': {
				filters: 'required',
				errors: { required: "Укажите автора сборника"}
			},
			'album[description]': {
				filters: 'required',
				errors: { required: "Укажите описание сборника"}
			}
		}
	}

	var $album = $('#album').idealforms( album_options ).data('idealforms');

	$('#genre').parent().children('.ideal-icon-invalid').addClass('custom-error').show();
	$('#genre').parent().parent().children('.ideal-error').html("Укажите жанр");
	$('#album_author').parent().addClass('valid');

	$('#fake-btn-2').click(function() {
		yey_album(); 
		return false;
	});

	function yey_album() {
		if ($album.isValid()) {
			if ($('#genres-group .genre .ideal-select-title').html() == "") {
				$('#genre').parent().addClass('invalid');
				$('#genre').parent().children('.ideal-icon-invalid').click();
				$('#genre').parent().parent().children('.ideal-error').show();
			}
			else {
				window.alert = function() { 
					$('#create-btn').addClass('btn-success').html("Зугрузка...").click(function () {
						$('.msg').html('Дождитесь создания сборника');
					});
				};
				$('#album').submit();
			}
		}
		else {
			$album.focusFirstInvalid();
		}
	}

	// ВАЛИДАЦИИ СОЗДАНИЯ КОЛЛЕКЦИИ
	var compilation_options = {
		inputs: {
			'compilation[c_name]': {
				filters: 'required',
				errors: { required: "Укажите название коллекции"}
			},
			'compilation[author]': {
				filters: 'required',
				errors: { required: "Укажите автора коллекции"}
			},
			'compilation[description]': {
				filters: 'required',
				errors: { required: "Укажите описание коллекции"}
			}
		}
	}

	var $compilation = $('#compilation').idealforms( compilation_options ).data('idealforms');

	$('#compilation_author').parent().addClass('valid');

	$('#fake-btn-3').click(function() {
		yey_compilation(); 
		return false;
	});

	function yey_compilation() {
		if ($compilation.isValid()) {
			window.alert = function() { 
				$('#create-btn').addClass('btn-success').html("Зугрузка...").click(function () {
					$('.msg').html('Дождитесь создания коллекции');
				});
			};
			$('#compilation').submit();
		}
		else {
			$compilation.focusFirstInvalid();
		}
	}

	// EXTENDING 
	$('.ideal-select-item').click(function() {
		if ($(this).html != "") {
			$('.ideal-error').hide();
			$(this).parent().parent().parent().parent().removeClass('invalid').addClass('valid');
			$(this).parent().parent().parent().parent().children('.ideal-icon-invalid').hide();
			$(this).parent().parent().parent().parent().children('.ideal-icon-valid').show();
		}
		else {
			$(this).parent().parent().parent().parent().removeClass('valid').addClass('invalid');
			$(this).parent().parent().parent().parent().children('.ideal-icon-valid').hide();
			$(this).parent().parent().parent().parent().children('.ideal-icon-invalid').show();
		}
	});

});
