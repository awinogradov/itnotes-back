class Wish < ActiveRecord::Base
  attr_accessible :author, :genre, :instrument, :user_id, :w_name, :finded, :comment

  belongs_to :user

  validates :author, :presence => true
  validates :genre, :presence => true
  validates :instrument, :presence => true
  validates :w_name, :presence => true
  validates :comment, :presence => true

  include Models::ItemsMethods::HistoryMethods
  
end
