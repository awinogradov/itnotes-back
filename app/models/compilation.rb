class Compilation < ActiveRecord::Base
  attr_accessible :active, :author, :c_name, :description, :genre, :sale, :cost, :composer_id, :group_id, 
                  :preferences, :paused, :locked

  has_many :albums
  belongs_to :group
  
  include Models::ItemsMethods::HistoryMethods
  include Models::ItemsMethods
  include Models::ItemsMethods::InstanceMethods

  def recost
    cost = 0
    self.albums.each do |a|
      album_cost = 0
      a.compositions.each do |c|
        album_cost += c.cost
      end
      a.update_attributes(cost: album_cost)
      cost += album_cost
    end
    self.update_attributes(cost: cost)
  end
end
