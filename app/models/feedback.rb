class Feedback < ActiveRecord::Base
  attr_accessible :body, :theme, :email

  validates :theme, :presence => true
  validates :email, :presence => true
  validates :body, :presence => true
  
  include Models::ItemsMethods::HistoryMethods
  
end
