class Account < ActiveRecord::Base
  attr_accessible :about, :birthday, :f_name, :l_name, :account_type, :user_id, :bill, :composer, :acepted
  
  has_many :apps
  belongs_to :user

  validates :f_name, :presence => true
  validates :l_name, :presence => true
  validates :birthday, :presence => true
  validates :about, :presence => true

  include Models::ItemsMethods::HistoryMethods
  
end
