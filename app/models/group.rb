class Group < ActiveRecord::Base
  attr_accessible :cover, :description, :g_name

  has_many :compilations
  has_many :albums
  has_many :compositions

end
