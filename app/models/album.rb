class Album < ActiveRecord::Base

  attr_accessible :a_name, :active, :b_count, :cost, :description, :genre, :v_count,
				  :cart_id, :author, :cover, :library_id, :preferences, :compilation_id, :sale, 
          :composer_id, :group_id, :paused, :locked

  mount_uploader :cover, CoverUploader
				  
  belongs_to :compilation
  has_many :compositions
  belongs_to :group

  include Models::ItemsMethods::HistoryMethods
  include Models::ItemsMethods
  include Models::ItemsMethods::InstanceMethods

  before_create :prepare

  def delete_from_anywhere
    if self.compositions.any?
      self.compositions.each do |composition|
        Composition.find(composition).update_attributes(album_id: 0)
      end
    end
    Album.delete(self)
  end

  def delete_from_library(library)
    self.compositions.each do |composition|
      Library.find(library).compositions.delete(composition)
    end
    Library.find(library).albums.delete(self)
  end

  def recost
    cost = 0
    self.compositions.each do |c|
      cost += c.cost
    end
    self.update_attributes(cost: cost)
  end

  def prepare
    self.cover = "/assets/no-cover.png" if self.cover.nil?
  end

end
