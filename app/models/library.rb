class Library < ActiveRecord::Base
  attr_accessible :user_id

  belongs_to :user
  has_many :compositions
  has_many :albums
  has_many :compilations
  
  include Models::ItemsMethods::HistoryMethods
end
