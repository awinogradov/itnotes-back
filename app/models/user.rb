class User < ActiveRecord::Base
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email, :password, :password_confirmation, :remember_me

  has_one :account
  has_one :cart
  has_one :library
  has_many :notifications
  has_many :wishes

  include Models::ItemsMethods::HistoryMethods
  
  after_create :add_instruments

  private

  	def add_instruments
      self.create_cart
      self.create_library
  	end
  	
end
