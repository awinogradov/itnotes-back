class Composition < ActiveRecord::Base
  attr_accessible :active, :author, :b_count, :c_name, :cost, :description, :genre, :v_count,
				  :cart_id, :album_id, :instrument, :library_id, :words, :c_type, :composer_id, :group_id,
				  :cover, :preferences, :paused, :locked, :audio

  mount_uploader :cover, CoverUploader
  mount_uploader :audio, AudioUploader

  belongs_to :album
  belongs_to :group
  
  scope :without_album, where(:album_id => 0)
  
  include Models::ItemsMethods::HistoryMethods
  include Models::ItemsMethods
  include Models::ItemsMethods::InstanceMethods

  before_create :prepare

  def delete_from_all
    File.delete(PDF + self.preferences[:print])
    File.delete(PREVIEWS + self.preferences[:preview])
    self.delete
  end

  def prepare
    self.cover = "/assets/no-cover.png" if self.cover.nil?
  end
end
