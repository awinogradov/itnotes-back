class Notification < ActiveRecord::Base
  attr_accessible :body, :title, :user_id
  belongs_to :user

  include Models::ItemsMethods::HistoryMethods
  
end
