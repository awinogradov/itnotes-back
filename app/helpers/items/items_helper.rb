#encoding: utf-8
module Items::ItemsHelper
  def item_cost(item)
    if item.free?
			content_tag :strong, "бесплатно"
		else
			"Цена: " + item.cost + "$"
    end
  end
end
