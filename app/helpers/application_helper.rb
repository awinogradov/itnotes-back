#encoding: utf-8
module ApplicationHelper

  def album_genre(album)
    unless album.genre.nil?
      truncate(album.genre, length: 17)
    else
      "Не указан"
    end
  end

  def composition_genre(composition)
    unless composition.genre.nil?
      composition.genre
    else
      album_genre(composition.album)
    end
  end

  def composition_album(composition)
    if composition.album.nil?
      "-"
    else
      link_to truncate(composition.album.a_name, length: 20), items_album_path(composition.album), title: composition.album.a_name
    end
  end

  def composition_cost(composition)
    if composition.free?
      content_tag :strong, "бесплатно"
    else
      composition.cost + "$"
    end
  end

  def item_badge(item)
    if !item.active?
      content_tag :span, "Wait", class: 'label label-important'
    elsif item.active?
      content_tag :span, "Sale", class: 'label label-success'
    elsif item.active? && item.paused?
      content_tag :span, "Paused", class:'label label-info'
    end
  end

  def user_navigation
		  content_tag(:div, (render partial: "layouts/nav"), class: "span2") if user_signed_in?
  end

  def compositions_select
    select_tag :compositions, options_from_collection_for_select(Composition.where(composer_id: current_user, album_id: 0), "id", "c_name"), placeholder: "Выберите композиции...", multiple: true, size: 10, class: "chzn-select"
  end

  def compilations_select
    select_tag :to, options_from_collection_for_select(Compilation.where(composer_id: current_user), "id", "c_name"), placeholder: "Выберите коллекцию...", multiple: false
  end

  def albums_select
    select_tag :to, options_from_collection_for_select(Album.where(composer_id: current_user, compilation_id: 0), "id", "a_name"), placeholder: "Выберите сборник...", multiple: false
  end

  def genres_select(selected=nil)
    select_tag :genre, options_from_collection_for_select(Genre.all, "g_name", "g_name"), placeholder: selected
  end

  def instruments_select(selected=nil)
    select_tag :instrument, options_from_collection_for_select(Instrument.all, "i_name", "i_name"), placeholder: selected
  end

  def cost_select
    cost = "<option>0</option>"
    100.times do |n|
      cost += "<option>#{n+1}</option>"
    end
    select_tag :cost, cost.html_safe
  end
end
