ThinkingSphinx::Index.define :compilation, :with => :active_record do
  indexes :c_name,:sortable => true
  indexes author
  indexes description
  indexes genre

  has active
  has paused
  has created_at
end