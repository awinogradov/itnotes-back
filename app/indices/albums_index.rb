ThinkingSphinx::Index.define :album, :with => :active_record do
  indexes :a_name, :sortable => true
  indexes author
  indexes description
  indexes genre

  has active
  has paused
  has created_at
end