ThinkingSphinx::Index.define :composition, :with => :active_record do
  indexes :c_name, :sortable => true
  indexes author
  indexes description
  indexes instrument
  indexes genre

  has active
  has paused
  has created_at
end