# encoding: utf-8
class WelcomeController < ApplicationController
  
  def main
  	redirect_to account_path if user_signed_in?
  end

  def shop
    @position = "Магазин"
    @title = "Магазин ITNotes"
    if user_signed_in?
      @page_size = "span10"
    else
      @page_size = "span12"
    end
  	# @best Лучшие альбомы
  	# @new Новые альбомы
  	# @free Бесплатные альбомы
  	# @popular_compositions Популярные композиции

    # NEW
    @compilations = Compilation.sale.order("created_at DESC").limit(5)
    @best = Album.sale.order("b_count DESC").limit(8)
    @new = Album.sale.order("created_at DESC").limit(8)
    @popular_compositions = Composition.without_album.sale.order("v_count DESC").limit(8)
    @composers = Composition.sale.where("author IS NOT NULL").select(:author).group(:author).limit(15)
    @instruments = Composition.sale.where("instrument IS NOT NULL").select(:instrument).group(:instrument).limit(15)
    @genres = Composition.sale.where("genre IS NOT NULL").select(:genre).group(:genre).limit(15)
  end
  
end


#ActionView::Template::Error (can't convert nil into String):
#    90:             %h6 Композиторы
#    91:             %ul
#    92:               - @composers.each do |composer|
#    93:                 %li= link_to composer.short_composer, show_category_path("Композиторы", composer.author)
#  lib/models/items_methods.rb:30:in `+'
#  lib/models/items_methods.rb:30:in `short_composer'
