# encoding: utf-8
class FeedbacksController < ApplicationController

	before_filter :authenticate_user!

  def index
  	@feedbacks = Feedback.all
  end

  def show
  	@feedback = Feedback.find(params[:id])
  end

  def new
    @position = "Обратная связь"
  	@feedback = Feedback.new(email: current_user.email)
  end

  def create
  	@feedback = Feedback.create(params[:feedback])
  	if !@feedback.save
  		render 'new'
  	end
  end

end
