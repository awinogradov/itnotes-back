# encoding: utf-8
class SearchController < ApplicationController

	def show
		@composers = Composition.sale.where("author IS NOT NULL").select(:author).group(:author).limit(15)
    	@instruments = Composition.sale.where("instrument IS NOT NULL").select(:instrument).group(:instrument).limit(15)
    	@genres = Composition.sale.where("genre IS NOT NULL").select(:genre).group(:genre).limit(15)
		if user_signed_in?
	      @page_size = "span10"
	    else
	      @page_size = "span12"
	    end	
	    @position = params[:category]
	    @item = params[:item]
	    @view = "album"
	    case params[:category]
	    	when "Жанры"
	    		@compositions = Composition.sale.where(genre: params[:item]).order("b_count DESC")
	    		@albums = Album.sale.where(genre: params[:item]).order("b_count DESC")
	    		@compilations = Compilation.sale.where(genre: params[:item]).order("b_count DESC")
	    		@view = "all"
	    	when "Инструменты"
	    		@compositions = Composition.sale.where(instrument: params[:item]).order("b_count DESC")
	    		@view = "composition"
	    	when "Композиторы"
	    		@compositions = Composition.sale.where(author: params[:item]).order("b_count DESC")
	    		@albums = Album.sale.where(author: params[:item]).order("b_count DESC")
	    		@compilations = Compilation.sale.where(author: params[:item]).order("b_count DESC")
	    		@view = "all"
	    	when "Коллекции"
	    		@view = "compilation"
	    		@items = Compilation.sale.order("created_at DESC")
	    	when "Лучшие сборники"
	    		@items = Album.sale.order("b_count DESC")
	    	when "Новые сборники"
	    		@items = Album.sale.order("created_at DESC")
	    	when "Популярные композиции"
	    		#TODO: Просмотр всех композиций
	    		@view = "composition"
	    	when "Все"
	    		@item = "Результаты по запросу: " + params[:search]
	    		@view = "all"
	    		@albums = ThinkingSphinx.search(params[:search], :classes => [Album], :with => {:active => '1', :paused => '0'})
	    		@compilations = ThinkingSphinx.search(params[:search], :classes => [Compilation], :with => {:active => '1', :paused => '0'})
	    		@compositions = ThinkingSphinx.search(params[:search], :classes => [Composition], :with => {:active => '1', :paused => '0'})
	    end
	    	
	end

end
