#encoding: utf-8
class HelpController < ApplicationController

	def create_composition
		@position = "Как создать композицию?"
	end

	def create_album
		@position = "Как создать сборник?"
	end

	def create_compilation
		@position = "Как создать коллекцию?"
	end

	def sale_all
		@position = "Как все это продать?"
	end
end
