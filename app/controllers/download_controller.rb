class DownloadController < ApplicationController

	before_filter :authenticate_user!

	def composition
		composition = Composition.find(params[:id])
		if composition.free?
			send_file(Rails.root.join("public", "assets", "pdf", composition.preferences[:print]), filename: "#{composition.c_name}(#{composition.author}).pdf")
		else 
			if current_user.library.compositions.exists?(composition)
				send_file(Rails.root.join("public", "assets", "pdf", composition.preferences[:print]), filename: "#{composition.c_name}(#{composition.author}).pdf")
			end
		end
	end

end
