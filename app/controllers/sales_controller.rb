#encoding: utf-8
class SalesController < ApplicationController
	
	before_filter :composer_validate

	def new
		@item = params[:item]
		case @item 
			when 'compilation'
				@compilation = Compilation.find(params[:id])
				@position = "Продажа коллекции"
				@cost = 0
				@compilation.albums.each do |a|
					album_cost = 0
					a.compositions.each do |c|
						album_cost += c.cost
					end
					@cost += album_cost
				end
				@sale = "<option>0</option>"
				10.times do |n|
					@sale += "<option>#{n+5}</option>"
				end
			when 'album'
				@album = Album.find(params[:id])
				@position = "Продажа альбома"
				@cost = 0
				@album.compositions.each do |c|
					@cost += c.cost
				end
				@sale = "<option>0</option>"
				10.times do |n|
					@sale += "<option>#{n+5}</option>"
				end
			when 'composition'
				@composition = Composition.find(params[:id])
				@position = "Продажа композиции"
		end	
	end

	def create
		@item = params[:item]
		case @item
			when 'compilation'
				@compilation = Compilation.find(params[:id])
				@cost = 0
				@compilation.albums.each do |a|
					album_cost = 0
					a.compositions.each do |c|
						album_cost += c.cost
						c.update_attributes(active: true)
					end
					a.update_attributes(cost: album_cost, active: true)
					@cost += album_cost
				end
				if params[:sale].present?
					@sale = @cost * (params[:sale].to_d / 100)
					@compilation.update_attributes(sale: params[:sale], cost: @cost - @sale, active: true)
				else
					@compilation.update_attributes(cost: @cost, active: true)
				end
				@compilation.save
				redirect_to controller: "users/composers", action: :show, id: current_user, type: "sale"
			when 'album'
				@album = Album.find(params[:id])
				@cost = 0
				@album.compositions.each do |c|
					@cost += c.cost
					c.update_attributes(active: true)
				end
				if params[:sale].present?
					@sale = @cost * (params[:sale].to_d / 100)
					@album.update_attributes(sale: params[:sale], cost: @cost - @sale, active: true)
				else
					@album.update_attributes(cost: @cost, active: true)
				end
				@album.save
				redirect_to controller: "users/composers", action: :show, id: current_user, type: "sale"
			when 'composition'
				Composition.find(params[:id]).update_attributes(active: true)
				redirect_to controller: "users/composers", action: :show, id: current_user, type: "sale"
		end
		
	end

	def pause
		@item = params[:item]
		case @item
			when 'compilation'
				@compilation = Compilation.find(params[:id])
				@compilation.albums.each do |a|
					a.compositions.each do |c|
						c.update_attributes(paused: true)
					end
					a.update_attributes(paused: true)
				end
				@compilation.update_attributes(paused: true)
			when 'album'
				@album = Album.find(params[:id])
				@album.compositions.each do |c|
					c.update_attributes(paused: true)
				end
				@album.update_attributes(paused: true)
			when 'composition'
				Composition.find(params[:id]).update_attributes(paused: true)
		end
		redirect_to controller: "users/composers", action: :show, id: current_user, type: "all"
	end

	def play
		@item = params[:item]
		case @item
			when 'compilation'
				@compilation = Compilation.find(params[:id])
				@compilation.albums.each do |a|
					a.compositions.each do |c|
						c.update_attributes(paused: false)
					end
					a.update_attributes(paused: false)
				end
				@compilation.update_attributes(paused: false)
			when 'album'
				@album = Album.find(params[:id])
				@album.compositions.each do |c|
					c.update_attributes(paused: false)
				end
				@album.update_attributes(paused: false)
			when 'composition'
				Composition.find(params[:id]).update_attributes(paused: false)
		end
		redirect_to controller: "users/composers", action: :show, id: current_user, type: "sale"
	end

end
