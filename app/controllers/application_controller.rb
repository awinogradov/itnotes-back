class ApplicationController < ActionController::Base
  protect_from_forgery

  def after_sign_in_path_for(resource)
  	stored_location_for(resource) || root_path
  end

  private
  	def composer_validate
  		if current_user.account.composer?
  			return true 
  		else 
  			redirect_to :back
  		end
    end

    def view_weight
      if user_signed_in?
        @page_size = "span10"
        @cover_size = "span3"
      else
        @page_size = "span12"
        @cover_size = "span2"
      end
    end

end
