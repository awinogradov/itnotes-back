#encoding: utf-8
class Users::ComposersController < ApplicationController

  before_filter :composer_validate, :except => [:new, :create]

  def new
  	@position = "Принятие соглашения"
  end

  def create
  	current_user.account.update_attributes(composer: true, acepted: true)
  	redirect_to action: :show, id: current_user, type: "all"
  end

  def show
  	if current_user.account.composer?
	  	case params[:type]
	  		when "all"
	  			@position = "Моя витрина | Все"
	  			@compilations = Compilation.where(composer_id: current_user)
	  			@albums = Album.where(composer_id: current_user, compilation_id: 0)
	  			@compositions = Composition.where(composer_id: current_user, album_id: 0)
	  		when "sale"
	  			@position = "Моя витрина | В продаже"
	  			@compilations = Compilation.where(composer_id: current_user, active: true)
	  			@albums = Album.where(composer_id: current_user, active: true, compilation_id: 0)
	  			@compositions = Composition.where(composer_id: current_user, active: true, album_id: 0)
	  		when "wait"
	  			@position = "Моя витрина | Ожидают продажи"
	  			@compilations = Compilation.where(composer_id: current_user, active: false)
	  			@albums = Album.where(composer_id: current_user, active: false, compilation_id: 0)
	  			@compositions = Composition.where(composer_id: current_user, active: false, album_id: 0)
	  	end
    else
      redirect_to :back
    end
  end

end
