#encoding: utf-8
class Users::NotificationsController < ApplicationController

	before_filter :authenticate_user!

	def index
		@position = "Мои сообщения"
		@notifys = current_user.notifications
	end

	def show
		@position = "Просмотр сообщения"
		@notify = current_user.notifications.find(params[:id])
		@notify.toggle!(:viewed) if !@notify.viewed?
	end

	def destroy
		current_user.notifications.find(params[:id]).delete
		redirect_to action: :index
	end
	
end