# encoding: utf-8
class Users::WishesController < ApplicationController

	before_filter :authenticate_user!

  def index
    @position = "Мои пожелания"
  	@wishes = current_user.wishes
  end

  def show
  end

  def new
    @position = "Новое пожелание"
  	@wish = Wish.new
  end

  def create
  	@wish = Wish.create(params[:wish])
  	if @wish.save
  		current_user.wishes << @wish
  		redirect_to action: :index
  	else 
  		render 'new'
  	end
  end

  def destroy
  	current_user.wishes.find(params[:id]).delete
  	redirect_to action: :index
  end
end
