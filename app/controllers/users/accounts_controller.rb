# encoding: utf-8
class Users::AccountsController < ApplicationController

	before_filter :authenticate_user!

	def new
		@account = Account.new
		@url = "users/accounts"
	end

	def create
		@account = current_user.create_account(params[:account])
		if @account.save
			#TODO: Отправка письма с подтверждением регистрации
			#TODO: Создание сообщение с приветствием
			redirect_to users_account_path(@account)
		else
			render :new
		end
	end

	def edit
		@account = current_user.account
		@url = "users/account"
	end

	def update
		if current_user.account.update_attributes(params[:account])
			redirect_to shop_path
		else
			render :edit
		end
	end

end
