class CartController < ApplicationController

	before_filter :authenticate_user!
	
	def show
		@cart = current_user.cart
	end
end
