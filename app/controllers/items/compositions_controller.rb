#encoding: utf-8
class Items::CompositionsController < Items::ItemsController

	before_filter :authenticate_user!, only: [:new, :create, :send_to_album]
	before_filter :composer_validate, only: [:new, :create, :send_to_album]

	require 'prawn'

	def index
		@position = "Мои композиции"
		@compositions = current_user.library.compositions
	end

	def show
    @composition = Composition.find(params[:id])
    @title = @composition.c_name
    @position = "Просмотр композиции"
    @inlib = false
    view_weight
    update_statistic(@composition)
	end

	def destroy
    case params[:from]
      when "all"
        @composition = Composition.find(params[:id])
        @composition.delete_from_all
        redirect_to "/users/composers/#{current_user.id}?type=wait"
      when "library"
        @composition = current_user.library.compositions.find(params[:id])
        current_user.library.compositions.delete(@composition)
        redirect_to action: :index
      else
        @composition = current_user.library.compositions.find(params[:id])
        current_user.library.compositions.delete(@composition)
        redirect_to action: :index
    end
	end

	def new 
		@position = "Создание новой композиции"
		@composition = Composition.new
	end

	def create
		if params[:composition][:pdf].present?
      @composition = Composition.new( c_name: params[:composition][:c_name],
                                      composer_id: current_user.id,
                                      genre: params[:genre].blank? ? params[:composition][:genre] : params[:genre],
                                      instrument: params[:instrument],
                                      description: params[:composition][:description],
                                      author: params[:composition][:author],
                                      cover: params[:composition][:file],
                                      #audio: params[:composition][:audio],
                                      cost: params[:cost],
                                      active: false,
                                      album_id: params[:composition][:album_id].blank? ? 0 : params[:composition][:album_id])
      
      @composition.preferences ||= Hash.new
      # Загружаем оригинальный PDF во временную директорию
      ext =  params[:composition][:pdf].original_filename.split('.')[1]
      filename = Guid.new.to_s + "." + ext.to_s
			File.open(TEMP + filename, "wb") {|f| f.write(params[:composition][:pdf].read)}

			# Создание pdf превью
			pdf_processing(@composition,TEMP + filename, 0, PREVIEWS + filename)
			# Конвертация первой страницы pdf в JPG для превью
      count = %x[identify -format %n #{PREVIEWS + filename}]
      preview_name = Guid.new.to_s
      if count.to_i > 1
        Kernel.system "convert -density 75 #{PREVIEWS + filename}[0] #{PREVIEWS + preview_name}.jpg"
        @composition.preferences[:preview] = preview_name + ".jpg"
      else
        @composition.preferences[:preview] = "/assets/no-preview.jpg"
      end
      File.delete(PREVIEWS + filename)

      # Создание печатной версии
      pdf_processing(@composition,TEMP + filename, 1, PDF + filename)
      File.delete(TEMP + filename)
			@composition.preferences[:print] = filename

			# Сохраняем композицию
      if @composition.save
        if params[:composition][:album_id].blank?
          redirect_to "/users/composers/#{current_user.id}?type=wait"
        else
          redirect_to edit_items_album_path(params[:composition][:album_id])
        end
      else
        if params[:composition][:album_id].blank?
          render :new
        else
          redirect_to edit_items_album_path(params[:composition][:album_id])
        end
      end
		else
      if params[:composition][:album_id].blank?
        render :new
      else
        redirect_to edit_items_album_path(params[:composition][:album_id])
      end
		end	
	end



	def send_to_album
		@composition = Composition.find(params[:id])
		@album = Album.find(params[:to])
		@album.compositions << @composition
		@album.recost
		redirect_to "/users/composers/#{current_user.id}?type=#{params[:send][:where]}"
  end


  private

    def update_statistic(composition)
      if user_signed_in?
        unless current_user.library.compositions.where(id: composition).exists?
          composition.increate_view
        else
          @inlib = true
        end
      end
    end






    ##########################################################################
    # КОНВЕРТЕР
    ##########################################################################

    # ОБРАБОТКА PDF
    # Если флаг равен 0 - создаем штамп и расставлем его на всех страницах, 8 раз по 5 штук в строке.
    # Далее ставим копирайты на всех старницах внизу страницы по центру. Создаем белый блок на всю ширину
    # страницы 550px и высотой 30px, ставим его с отступом от нижнего края в 20px и от левого 0, 
    # а сверху него пишем текст.
    #
    # Парметры:
    #   input - файл шаблона, исходный загруженный pdf
    #   flag - тип обработки
    #   output - обработанный файл
    #
    def pdf_processing(composition,input, flag, output)
      if flag == 0
        Prawn::Document.new(:template => input) do 
            	create_stamp("itnotes") do
              		image LOGO, :scale => 0.5
            	end
	            page_count.times do |n|
	              	go_to_page(n + 1)
	              	8.times do |l|
	                	y = l*100
	                	5.times do |m|
	                  		x = (m*100)+40
	                  		transparent(0.3) { stamp_at "itnotes", [x, -y] }
	                	end
	              	end
	            end 
          	render_file output
        end
      else
        template = Prawn::Document.new(template: input) 

        Prawn::Document.new do 
          
          move_down 150

          image BIG_LOGO, scale: 0.6, position: :center

          move_down 200
          font Rails.root.join('public', 'assets',"arial.ttf")
          text "Композиция: #{composition.c_name}", align: :right
          text "Автор: #{composition.author}", align: :right

          move_down 100
          text "Приобретено в магазине ITNotes", align: :center
          text "www.itnot.es", align: :center, valign: :bottom


          template.page_count.times do |n|
            start_new_page(template: input, template_page: n + 1)
          end

          encrypt_document(owner_password: :random,
                          :permissions => { print_document: true,
                                            modify_contents: false,
                                            copy_contents: false,
                                            modify_annotations: false })
          render_file output
        end
      end
    end

end
