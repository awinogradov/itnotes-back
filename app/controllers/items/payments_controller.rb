#encoding: utf-8
class Items::PaymentsController < ApplicationController

	before_filter :authenticate_user!

	require "digest"

	def create
		case params[:type]
			when "free"
				case params[:what]
					when "compilation"
						@compilation = Compilation.find(params[:id])
						current_user.library.compilations << @compilation
						@compilation.increate_buy
							@compilation.albums.each do |album|
								current_user.library.albums << album
								album.increate_buy
								album.compositions.each do |composition|
									current_user.library.compositions << composition
									composition.increate_buy
								end
							end 
						redirect_to items_compilations_path
					when "album"
						@album = Album.find(params[:id])
						current_user.library.albums << @album
						@album.increate_buy
						@album.compositions.each do |composition|
							current_user.library.compositions << composition
							composition.increate_buy
						end 
						redirect_to items_albums_path
					when "composition"
						@composition = Composition.find(params[:id])
						current_user.library.compositions << @composition
						@composition.increate_buy
						redirect_to items_compositions_path
				end
			when "paid"
				# Параметры для РОБОКАССЫ
				#@server = "http://test.robokassa.ru/Index.aspx?"
				@MrchLogin = "abakumovigor"
				@Pwd = "0a10a20a30a4"
				@Shp_item = "notes_payments_transaction"
				@IncCurrLabel = "WMZM"
				@Culture = "ru"
				#@Shp_item = params[:id]
				order = Order.create(what: params[:what], ids: params[:id])
				@InvId = order.id #params[:id]
				case params[:what]
					when "compilation"
						compilation = Compilation.find(params[:id])
						@OutSumm = compilation.cost * 30
						@SignatureValue = Digest::MD5.hexdigest("#{@MrchLogin}:#{@OutSumm}:#{@InvId}:#{@Pwd}:Shp_item=#{@Shp_item}")
						@Desc = "Оплата издания #{compilation.c_name}"
						@What = "Коллекция"
						@Name = compilation.c_name
					when "album"
						album = Album.find(params[:id])
						@OutSumm = album.cost * 30
						@SignatureValue = Digest::MD5.hexdigest("#{@MrchLogin}:#{@OutSumm}:#{@InvId}:#{@Pwd}:Shp_item=#{@Shp_item}")
						@Desc = "Оплата альбома #{album.a_name}"
						@What = "Сборник"
						@Name = album.a_name
					when "composition"
						composition = Composition.find(params[:id])
						@OutSumm = composition.cost * 30
						@SignatureValue = Digest::MD5.hexdigest("#{@MrchLogin}:#{@OutSumm}:#{@InvId}:#{@Pwd}:Shp_item=#{@Shp_item}")
						@Desc = "Оплата композиции #{composition.c_name}"
						@What = "Композиция"
						@Name = composition.c_name
				end
				order.cost = @OutSumm
				order.md5 = @SignatureValue
				order.save
				#@params_string = "MrchLogin=#{@MrchLogin}&OutSumm=#{@OutSumm}&InvId=#{@InvId}&SignatureValue=#{@SignatureValue}"
				#redirect_to @server + @params_string
		end
	end


	def success
		order = Order.find(params[:InvId])
		request_md5 = Digest::MD5.hexdigest("#{params[:OutSumm]}:#{params[:InvId]}:0a10a20a30a40a5:Shp_item=#{params[:Shp_item]}")
		if !order.nil? && !params[:SignatureValue].blank?
			order.status = true
			order.save
			case order.what
				when "compilation"
					@compilation = Compilation.find(order.ids)
					current_user.library.compilations << @compilation
					@compilation.increate_buy
					@compilation.albums.each do |album|
						current_user.library.albums << album
						album.increate_buy
						album.compositions.each do |composition|
							current_user.library.compositions << composition
							composition.increate_buy
						end
					end
					composer = User.find(@compilation.composer_id)
					composer.account.bill += compilation.cost - compilation.cost * 0.33
					composer.account.save 
					redirect_to items_compilations_path
				when "album"
					@album = Album.find(order.ids)
					current_user.library.albums << @album
					@album.increate_buy
					@album.compositions.each do |composition|
						current_user.library.compositions << composition
						composition.increate_buy
					end 
					composer = User.find(@album.composer_id)
					composer.account.bill += album.cost - album.cost * 0.33
					composer.account.save
					redirect_to items_albums_path
				when "composition"
					@composition = Composition.find(order.ids)
					current_user.library.compositions << @composition
					@composition.increate_buy
					composer = User.find(@composition.composer_id)
					composer.account.bill += @composition.cost - @composition.cost * 0.33
					composer.account.save
					redirect_to items_compositions_path
			end 
		else
			redirect_to action: :fail
		end
	end

	def fail
		
	end
end
