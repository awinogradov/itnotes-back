class Items::ItemsController < ApplicationController

	LOGO = Rails.root.join('public', 'assets', 'logo_mini.png')
	BIG_LOGO = Rails.root.join('public', 'assets', 'logo_big.png')
	COVERS = Rails.root.join('public', 'assets', 'covers')
	PREVIEWS = Rails.root.join('public', 'assets', 'previews')
	AUDIO = Rails.root.join('public', 'assets', 'audio')
	PDF = Rails.root.join('public', 'assets', 'pdf')
	TEMP = Rails.root.join('public', 'assets', 'temp')
	
end
