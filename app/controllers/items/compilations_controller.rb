#encoding: utf-8
class Items::CompilationsController < Items::ItemsController
  
before_filter :authenticate_user!, :except => :show
before_filter :composer_validate, :only => [:new, :create]

  def index
  	@position = "Мои коллекции"
  	@compilations = current_user.library.compilations
  end

  def show
  	if user_signed_in?
	    @page_size = "span10"
	    @cover_size = "span3"
	else
	    @page_size = "span12"
	    @cover_size = "span2"
	end
  	@compilation = Compilation.find(params[:id])
  	@position = "Просмотр коллекции"
	@inlib = false
	if user_signed_in?
		if !current_user.library.compilations.where(id: @compilation).exists?
			@compilation.increate_view
		else
			@inlib = true
		end
	end
  end

  def destroy
  	if params[:from].present?
			case params[:from]
				when "all"
					Compilation.find(params[:id]).albums.each do |album|
				  		album.compilation_id = 0
				  		album.save
				  	end
				    compilation = Compilation.find(params[:id])
				  	Compilation.find(params[:id]).delete
				    redirect_to "/users/composers/#{current_user.id}?type=all"
				when "library"
					@compilation = current_user.library.compilations.find(params[:id])
					@compilation.albums.each do |album|
						album.compositions.each do |composition|
							current_user.library.compositions.delete(composition)
						end
						current_user.library.albums.delete(album)
					end
					current_user.library.compilations.delete(@compilation)
					redirect_to action: :index
			end
	else
	  	@compilation = current_user.library.compilations.find(params[:id])
		@compilation.albums.each do |album|
			album.compositions.each do |composition|
				current_user.library.compositions.delete(composition)
			end
			current_user.library.albums.delete(album)
		end
		current_user.library.compilations.delete(@compilation)
		redirect_to action: :index
	end
  end

  def new
  	@position = "Создание новой коллекции"
  	@compilation = Compilation.new
  end

  def create
  	@compilation = Compilation.create(params[:compilation])
  	@compilation.composer_id = current_user.id
  	if @compilation.save
  		redirect_to "/users/composers/#{current_user.id}?type=wait"
  	else
  		render :new
  	end
  end

  def add_albums
  	@compilation = Compilation.find(params[:id])
  	params[:albums].each do |id|
	  	@compilation.albums << Album.find(id)
	end
	cost = 0
	@compilation.albums.each do |a|
		album_cost = 0
		a.compositions.each do |c|
			album_cost += c.cost
		end
		a.update_attributes(cost: album_cost)
		cost += album_cost
	end
	@compilation.update_attributes(cost: cost)
	@compilation.save
	redirect_to "/users/composers/#{current_user.id}?type=#{params[:upload][:where]}"
  end

end
