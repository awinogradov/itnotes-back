#encoding: utf-8
class Items::AlbumsController < Items::ItemsController

	before_filter :authenticate_user!, :except => :show
	before_filter :composer_validate, :only => [:new, :create]
  before_filter :view_weight, only: [:show, :edit]
  before_filter :load_resources, only: [:new, :create]
	
	def index
		@position = "Мои сборники"
		@albums = current_user.library.albums
	end

	def show
		@album = Album.find(params[:id])
		@title = @album.a_name
		@position = "Просмотр сборника"
    update_statistic(@album)
	end

	def new
		@position = "Создание нового сборника"
	end

	def create
		@album = Album.new(a_name: params[:album][:a_name],
                       composer_id: current_user.id,
                       genre: params[:genre],
                       description: params[:album][:description],
                       author: params[:album][:author],
                       cover: params[:album][:file])
		if @album.save
      redirect_to "/users/composers/#{current_user.id}?type=wait"
    else
      render :new
    end
  end

  def edit
    @album = Album.find(params[:id])
    @position = "Редактирование сборника"
  end

  def update
    @album = Album.find(params[:id])
    if @album.update_attributes(params[:album])
      redirect_to action: :edit
    end
  end

  def destroy
    case params[:from]
      when "all"
        @album = Album.find(params[:id])
        @album.delete_from_anywhere
        redirect_to "/users/composers/#{current_user.id}?type=all"
      when "library"
        @album = current_user.library.albums.find(params[:id])
        @album.delete_from_library(current_user.library)
        redirect_to action: :index
      else
        @album = current_user.library.albums.find(params[:id])
        @album.delete_from_library(current_user.library)
        redirect_to action: :index
    end
  end

	def send_to_compilation
		@album = Album.find(params[:id])
		@compilation = Compilation.find(params[:to])
		@compilation.albums << @album
    @compilation.recost
		redirect_to "/users/composers/#{current_user.id}?type=#{params[:send][:where]}"
	end

	def add_compositions
		@album = Album.find(params[:id])
		params[:compositions].each do |id|
	  	@album.compositions << Composition.find(id)
	  end
		@album.recost
		redirect_to "/users/composers/#{current_user.id}?type=#{params[:upload][:where]}"
  end

  protected

    def load_resources
      @inlib = false
    end

    def update_statistic(album)
      if user_signed_in?
        unless current_user.library.albums.where(id: album).exists?
          album.increate_view
        else
          @inlib = true
        end
      end
    end

end
