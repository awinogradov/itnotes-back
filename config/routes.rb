ItnotesBack::Application.routes.draw do

  # MAIN PAGE
    root :to => 'welcome#shop'
  #END

  # USERS
    devise_for :users, :controllers => {:registrations => "users/registrations"} 
    namespace :users do
      resources :accounts 
      resources :composers, :only => [:new, :show, :create, :destroy]
      resources :notifications, :except => [:edit, :update]
      resources :wishes
    end
  # END

  # ALBUMS AND COMPOSITIONS
    namespace :items do
      resources :compilations do
        member do
          post '/add' => 'compilations#add_albums', as: 'add'
        end
      end
      resources :albums do
        member do
          post '/send' => 'albums#send_to_compilation', as: 'send'
          post '/add' => 'albums#add_compositions', as: 'add'
        end
      end
      resources :compositions do
        member do 
          post '/send' => 'compositions#send_to_album', as: 'send'
        end
      end
      resources :groups
      # INIT PAYMENT
        post "/payments/create" => "payments#create"
      # PAID - ROBOKASSA
        get "/payments/success" => "payments#success"
        get "/payments/fail" => "payments#fail"
      # END
    end
    # WORK WITH SHOP
      get "/sale/:item/:id" => "sales#new", as: 'sale'
      post "/sale/send/:item/:id" => "sales#create", as: 'send_sale'
      post "/sale/pause/:item/:id" => "sales#pause", as: 'pause_sale'
      post "/sale/play/:item/:id" => "sales#play", as: 'play_sale'
    # END
  # END

  # INFORMATION
    resources :feedbacks
  # END

  # DOWNLOAD
    post '/composition/:id' => 'download#composition', as: 'download_composition'
  # END

  # HELP
    get '/help/create_composition' => 'help#create_composition', as: 'create_composition_help'
    get '/help/create_album' => 'help#create_album', as: 'create_album_help'
    get '/help/create_compilation' => 'help#create_compilation', as: 'create_compilation_help'
    get '/help/sale_all' => 'help#sale_all', as: 'sale_all_help'
  # END
  
  # SEARCH
    get '/:category/:item' => 'search#show', as: 'show_category'
  # END

end
