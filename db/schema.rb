# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130405150334) do

  create_table "accounts", :force => true do |t|
    t.integer  "user_id"
    t.string   "f_name"
    t.string   "l_name"
    t.date     "birthday"
    t.text     "about"
    t.integer  "account_type"
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
    t.boolean  "news",                                       :default => true
    t.boolean  "composer",                                   :default => false
    t.boolean  "acepted",                                    :default => false
    t.decimal  "bill",         :precision => 6, :scale => 2, :default => 0.0,   :null => false
  end

  create_table "activities", :force => true do |t|
    t.text     "message"
    t.string   "from"
    t.string   "character"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "albums", :force => true do |t|
    t.string   "a_name"
    t.string   "author"
    t.string   "genre"
    t.text     "description"
    t.decimal  "cost",           :precision => 6, :scale => 2, :default => 0.0,   :null => false
    t.integer  "b_count",                                      :default => 0
    t.integer  "v_count",                                      :default => 0
    t.integer  "cart_id",                                      :default => 0
    t.datetime "created_at",                                                      :null => false
    t.datetime "updated_at",                                                      :null => false
    t.text     "cover"
    t.integer  "library_id"
    t.text     "preferences"
    t.boolean  "active",                                       :default => false
    t.integer  "compilation_id",                               :default => 0
    t.integer  "sale",                                         :default => 0
    t.integer  "group_id"
    t.integer  "composer_id"
    t.boolean  "paused",                                       :default => false
    t.boolean  "locked",                                       :default => false
  end

  create_table "carts", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "compilations", :force => true do |t|
    t.string   "c_name"
    t.text     "description"
    t.boolean  "active",                                    :default => false
    t.string   "author"
    t.string   "genre"
    t.datetime "created_at",                                                   :null => false
    t.datetime "updated_at",                                                   :null => false
    t.integer  "library_id"
    t.decimal  "cost",        :precision => 6, :scale => 2, :default => 0.0,   :null => false
    t.integer  "v_count",                                   :default => 0
    t.integer  "b_count",                                   :default => 0
    t.integer  "sale",                                      :default => 0
    t.integer  "group_id"
    t.integer  "composer_id"
    t.text     "preferences"
    t.boolean  "paused",                                    :default => false
    t.boolean  "locked",                                    :default => false
  end

  create_table "compositions", :force => true do |t|
    t.string   "c_name"
    t.string   "author"
    t.decimal  "cost",        :precision => 6, :scale => 2, :default => 0.0,                   :null => false
    t.text     "description"
    t.integer  "b_count",                                   :default => 0
    t.integer  "v_count",                                   :default => 0
    t.boolean  "active",                                    :default => false
    t.string   "genre"
    t.integer  "cart_id",                                   :default => 0
    t.integer  "album_id",                                  :default => 0
    t.datetime "created_at",                                                                   :null => false
    t.datetime "updated_at",                                                                   :null => false
    t.string   "instrument"
    t.text     "cover",                                     :default => "assets/no-cover.png"
    t.integer  "library_id"
    t.integer  "c_type"
    t.text     "preferences"
    t.integer  "group_id"
    t.integer  "composer_id"
    t.boolean  "paused",                                    :default => false
    t.boolean  "locked",                                    :default => false
    t.string   "audio"
  end

  create_table "feedbacks", :force => true do |t|
    t.string   "theme"
    t.text     "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "email"
  end

  create_table "genres", :force => true do |t|
    t.string   "g_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "groups", :force => true do |t|
    t.string   "g_name"
    t.text     "description"
    t.string   "cover"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "instruments", :force => true do |t|
    t.string   "i_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "libraries", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "notifications", :force => true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "body"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "viewed",     :default => false
  end

  create_table "orders", :force => true do |t|
    t.string   "what"
    t.string   "ids"
    t.string   "md5"
    t.decimal  "cost",       :precision => 6, :scale => 2, :default => 0.0,   :null => false
    t.boolean  "status",                                   :default => false
    t.datetime "created_at",                                                  :null => false
    t.datetime "updated_at",                                                  :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "wishes", :force => true do |t|
    t.string   "w_name"
    t.string   "author"
    t.string   "instrument"
    t.string   "genre"
    t.integer  "user_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "finded",     :default => false
    t.text     "comment"
  end

end
