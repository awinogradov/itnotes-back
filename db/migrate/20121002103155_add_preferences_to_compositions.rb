class AddPreferencesToCompositions < ActiveRecord::Migration
  def change
    add_column :compositions, :preferences, :text
  end
end
