class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :g_name
      t.text :description
      t.string :cover

      t.timestamps
    end
  end
end
