class AddAudioToCompositions < ActiveRecord::Migration
  def change
    add_column :compositions, :audio, :string
  end
end
