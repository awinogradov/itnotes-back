class AddGroupIdAndComposerIdToAlbums < ActiveRecord::Migration
  def change
    add_column :albums, :group_id, :integer
    add_column :albums, :composer_id, :integer
  end
end
