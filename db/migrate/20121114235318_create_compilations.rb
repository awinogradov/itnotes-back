class CreateCompilations < ActiveRecord::Migration
  def change
    create_table :compilations do |t|
      t.string :c_name
      t.text :description
      t.boolean :active, default: false
      t.string :author
      t.string :genre

      t.timestamps
    end
  end
end
