class AddLibraryToCompilations < ActiveRecord::Migration
  def change
    add_column :compilations, :library_id, :integer
  end
end
