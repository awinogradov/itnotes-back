class CreateCompositions < ActiveRecord::Migration
  def change
    create_table :compositions do |t|
      t.string :c_name
      t.string :author
      t.decimal :cost, :precision => 6, :scale => 2, :null => false, :default => 0
      t.text :description
      t.integer :b_count, :default => 0
      t.integer :v_count, :default => 0
      t.boolean :active, :default => false
      t.string :genre 
      t.integer :cart_id, :default => 0 
      t.integer :album_id, :default => 0

      t.timestamps
    end
  end
end
