class AddCostToCompilations < ActiveRecord::Migration
  def change
    add_column :compilations, :cost, :decimal, :precision => 6, :scale => 2, :null => false, :default => 0
  end
end
