class AddCompilationToAlbums < ActiveRecord::Migration
  def change
    add_column :albums, :compilation_id, :integer, default: 0
  end
end
