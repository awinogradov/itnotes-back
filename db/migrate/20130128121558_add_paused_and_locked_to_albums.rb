class AddPausedAndLockedToAlbums < ActiveRecord::Migration
  def change
    add_column :albums, :paused, :boolean, default: false
    add_column :albums, :locked, :boolean, default: false
  end
end
