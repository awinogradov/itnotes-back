class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.integer :user_id
      t.string :f_name
      t.string :l_name
      t.date :birthday
      t.text :about
      t.integer :account_type

      t.timestamps
    end
  end
end
