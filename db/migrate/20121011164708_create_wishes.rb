class CreateWishes < ActiveRecord::Migration
  def change
    create_table :wishes do |t|
      t.string :w_name
      t.string :author
      t.string :instrument
      t.string :genre
      t.integer :user_id

      t.timestamps
    end
  end
end
