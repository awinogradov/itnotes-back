class AddPausedAndLockedToCompilations < ActiveRecord::Migration
  def change
    add_column :compilations, :paused, :boolean, default: false
    add_column :compilations, :locked, :boolean, default: false
  end
end
