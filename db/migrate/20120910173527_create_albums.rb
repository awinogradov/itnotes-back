class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.string :a_name
      t.string :author
      t.string :genre
      t.text :description
      t.decimal :cost, :precision => 6, :scale => 2, :null => false, :default => 0
      t.integer :b_count, :default => 0
      t.integer :v_count, :default => 0
      t.integer :cart_id, :default => 0 

      t.timestamps
    end
  end
end
