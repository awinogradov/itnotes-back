class AddInstrumentToCompositions < ActiveRecord::Migration
  def change
    add_column :compositions, :instrument, :string
  end
end
