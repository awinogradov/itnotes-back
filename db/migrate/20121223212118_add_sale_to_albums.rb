class AddSaleToAlbums < ActiveRecord::Migration
  def change
    add_column :albums, :sale, :integer, default: 0
  end
end
