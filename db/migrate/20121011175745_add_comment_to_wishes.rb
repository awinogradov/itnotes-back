class AddCommentToWishes < ActiveRecord::Migration
  def change
    add_column :wishes, :comment, :text
  end
end
