class AddGroupIdAndComposerIdToCompilations < ActiveRecord::Migration
  def change
    add_column :compilations, :group_id, :integer
    add_column :compilations, :composer_id, :integer
  end
end
