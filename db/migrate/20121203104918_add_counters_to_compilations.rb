class AddCountersToCompilations < ActiveRecord::Migration
  def change
    add_column :compilations, :v_count, :integer, default: 0
    add_column :compilations, :b_count, :integer, default: 0
  end
end
