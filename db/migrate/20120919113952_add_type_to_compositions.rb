class AddTypeToCompositions < ActiveRecord::Migration
  def change
    add_column :compositions, :c_type, :integer
  end
end
