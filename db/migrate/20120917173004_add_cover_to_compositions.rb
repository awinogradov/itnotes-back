class AddCoverToCompositions < ActiveRecord::Migration
  def change
    add_column :compositions, :cover, :text, :default => "assets/no-cover.png"
  end
end
