class AddFindedToWishes < ActiveRecord::Migration
  def change
    add_column :wishes, :finded, :boolean, default: false
  end
end
