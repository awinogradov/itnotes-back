class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :what
      t.string :ids
      t.string :md5
      t.decimal :cost, :precision => 6, :scale => 2, :null => false, :default => 0
      t.boolean :status, default: false

      t.timestamps
    end
  end
end
