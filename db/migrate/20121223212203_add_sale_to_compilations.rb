class AddSaleToCompilations < ActiveRecord::Migration
  def change
    add_column :compilations, :sale, :integer, default: 0
  end
end
