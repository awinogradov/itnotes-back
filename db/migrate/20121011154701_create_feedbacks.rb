class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :theme
      t.text :body

      t.timestamps
    end
  end
end
