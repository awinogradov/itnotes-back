class AddPreferencesToAlbums < ActiveRecord::Migration
  def change
    add_column :albums, :preferences, :text
  end
end
