class AddComposerAndAceptedAndBillToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :composer, :boolean, default: false
    add_column :accounts, :acepted, :boolean, default: false
    add_column :accounts, :bill, :decimal, :precision => 6, :scale => 2, :null => false, :default => 0
  end
end
