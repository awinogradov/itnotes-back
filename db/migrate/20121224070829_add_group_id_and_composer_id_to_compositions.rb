class AddGroupIdAndComposerIdToCompositions < ActiveRecord::Migration
  def change
    add_column :compositions, :group_id, :integer
    add_column :compositions, :composer_id, :integer
  end
end
