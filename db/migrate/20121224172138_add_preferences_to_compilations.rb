class AddPreferencesToCompilations < ActiveRecord::Migration
  def change
    add_column :compilations, :preferences, :text
  end
end
