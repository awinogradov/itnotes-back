class AddPausedAndLockedToCompositions < ActiveRecord::Migration
  def change
    add_column :compositions, :paused, :boolean, default: false
    add_column :compositions, :locked, :boolean, default: false
  end
end
