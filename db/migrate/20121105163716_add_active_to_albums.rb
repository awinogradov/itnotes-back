class AddActiveToAlbums < ActiveRecord::Migration
  def change
    add_column :albums, :active, :boolean, :default => false
  end
end
