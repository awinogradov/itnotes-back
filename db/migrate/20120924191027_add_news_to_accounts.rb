class AddNewsToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :news, :boolean, :default => true
  end
end
